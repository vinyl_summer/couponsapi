import datetime
import re

import requests
from bs4 import BeautifulSoup
import uvicorn
import os

from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every


class Coupon:
    def __init__(self, meal, price):
        self.meal = meal
        self.price = price


coupon_5050 = Coupon(None, None)
app = FastAPI()


@app.on_event("startup")
@repeat_every(seconds=60 * 60 * 2)
async def get_5050():
    try:
        r = requests.get('https://www.kfc.ru/promo/crazydays',
                         headers={
                             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'})
    except requests.exceptions.HTTPError as error:
        print(error)
    document = BeautifulSoup(r.text, 'html.parser')
    document_title = document.find_all('title')[0].text
    coupon_5050.meal = re.search('получи(.+?)по цене', document_title)[1].strip()


@app.get("/5050")
async def root():
    return {"meal": f"{coupon_5050.meal}",
            "price": f"{coupon_5050.price}",
            "date": f"{datetime.datetime.now()}"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=int(os.environ['PORT']))
